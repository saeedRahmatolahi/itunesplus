//
//  UITableViewExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 2/1/2564 BE.
//

import Foundation
import UIKit

extension UITableView {
    func reloadingData() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}
