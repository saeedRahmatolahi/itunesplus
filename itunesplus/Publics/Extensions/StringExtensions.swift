//
//  StringExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation

extension String {
    
    func randomString(_ length: Int = 20) -> String {
        let characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomCharacters = (0..<length).map{_ in characters.randomElement()!}
        return String(randomCharacters)
    }
    
    /// detects if the String characters are more than 2
    /// - Returns: is more than 2 characters or not (more than 2 characters : true  )
    func isMoreThanTwo() -> Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).count > 2
    }

    func toDate(_ style : DateFormatter.Style = .medium) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .posixENUS
        dateFormatter.dateFormat = .dateFormatLongT
        let formatter2 = DateFormatter()
        formatter2.dateStyle = style
        formatter2.locale = .posixENUS
        return formatter2.string(from: dateFormatter.date(from: self) ?? Date())
    }
    
    static let dateFormatLongT = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZ"
}
