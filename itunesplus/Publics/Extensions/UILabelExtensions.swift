//
//  UILabelExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation
import UIKit

extension UILabel {
    func setText(_ text : String?) {
        self.text = text.unwrapString()
    }
}
