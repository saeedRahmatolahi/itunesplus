//
//  CGSizeExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation
import UIKit

extension CGSize {
    static let GridSize = CGSize(width: .screenWidth/2 - 15, height: 100)
}
