//
//  DoubleExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 2/1/2564 BE.
//

import Foundation

extension Double {
    var localeCurrency: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter.string(from: self as NSNumber) ?? ""
    }
}
