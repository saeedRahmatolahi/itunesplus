//
//  OtherExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 2/1/2564 BE.
//

import Foundation

extension Locale {
    static let posixENUS = Locale(identifier: "en_US_POSIX")
}
