//
//  OptionalsExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation

extension Optional where Wrapped == String {
    func unwrapString() -> String {
        if let str = self {
            return str
        } else {
            return ""
        }
    }
}


extension Optional where Wrapped == Double {
    func unwrapDouble() -> Double {
        if let double = self {
            return double
        } else {
            return 0.0
        }
    }
}
