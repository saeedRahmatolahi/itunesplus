//
//  UIViewControllerExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation
import UIKit

extension UIViewController {
    func performingSegue(_ segueName : String) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: segueName, sender: self)
        }
    }
}
