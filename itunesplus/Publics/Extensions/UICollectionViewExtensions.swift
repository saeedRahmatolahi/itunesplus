//
//  UICollectionViewExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerCVCell(_ name : String) {
        self.register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
    }
    
    func reloadingData() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}
