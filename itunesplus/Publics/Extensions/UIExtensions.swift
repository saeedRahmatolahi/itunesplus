//
//  UIExtensions.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation
import UIKit

extension UISegmentedControl {
    func replaceSegments(_ segments: Array<String>) {
        self.removeAllSegments()
        for segment in segments {
            self.insertSegment(withTitle: segment, at: self.numberOfSegments, animated: false)
        }
    }
    
    func selectSegment(_ index : Int) {
        self.selectedSegmentIndex = index
    }
}
