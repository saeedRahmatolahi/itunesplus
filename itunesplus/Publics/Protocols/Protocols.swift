//
//  Protocols.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation

protocol searchFilterProtocol : NSObjectProtocol {
    func searchText(_ text : String,_ selectedCategory : String)
}
