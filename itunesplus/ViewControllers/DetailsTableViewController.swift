//
//  DetailsTableViewController.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 2/1/2564 BE.
//

import UIKit

class DetailsTableViewController: UITableViewController {

    var pageModel : Results? = nil
    var data = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchDetails()
        
    }
    
    func fetchDetails() {
        self.data.append(pageModel?.artworkUrl100 ?? "")
        self.data.append(pageModel?.collectionName ?? "")
        self.data.append(pageModel?.releaseDate.unwrapString().toDate() ?? "")
        self.data.append(pageModel?.collectionPrice.unwrapDouble().localeCurrency ?? "")
        self.data.append(pageModel?.artworkUrl30 ?? "")
        self.data.append(pageModel?.artworkUrl60 ?? "")
        self.data.append(pageModel?.artistName ?? "")
        self.tableView.reloadingData()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.setText(self.data[indexPath.row])

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
