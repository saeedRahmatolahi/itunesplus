//
//  SearchPageViewController.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import UIKit

class SearchPageViewController: UIViewController {
    
    @IBOutlet weak var searchView: SearchFilterView!
    @IBOutlet weak var searchResultsCV: UICollectionView!
    var responseModel : ResponseModel? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    
    func setupViews() {
        self.searchResultsCV.registerCVCell(cell.resultCell)
        self.searchView.searchFilterDelegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? DetailsTableViewController else {return}
        vc.pageModel = self.responseModel?.results[self.searchResultsCV.indexPathsForSelectedItems?[0].item ?? 0]
    }

}

extension SearchPageViewController : searchFilterProtocol {
    func searchText(_ text: String, _ selectedCategory: String) {
        guard let url = URL(string: url.searchUrl + text + url.wrapperType + selectedCategory) else {return}
        Webservices.get(url) { (model :ResponseModel?,error) in
            self.responseModel = model
            self.searchResultsCV.reloadingData()
        }
    }
    
}


extension SearchPageViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.responseModel?.results.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cell.resultCell, for: indexPath) as? ResultCell else {return UICollectionViewCell()}
        
        cell.fetchData(self.responseModel?.results[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performingSegue(sg.showDetails)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .GridSize
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // getting the scroll offset
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            print("End Of Scroll Getting New Data and Append to the current data")
        }
    }
    
}
