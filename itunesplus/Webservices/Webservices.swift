//
//  Webservices.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation
#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

class Webservices {
    static func get<model:Decodable>(_ url : URL,completion : @escaping(model?,Error?) -> Void) {
        let semaphore = DispatchSemaphore (value: 0)
        
        var request = URLRequest(url: url,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(nil,error)
                semaphore.signal()
                return
            }
            do {
                
                let res = try JSONDecoder().decode(model.self, from: data)
                completion(res,nil)
            } catch {
                completion(nil,error)
            }
            
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
    }
    
}
