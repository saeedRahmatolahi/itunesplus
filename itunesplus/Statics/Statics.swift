//
//  Statics.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation

struct Statics {
    static var searchTypes = ["Movies", "Music", "Apps", "Books"]
}
