//
//  URLS.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import Foundation

struct URLS {
    static let baseUrl = "https://itunes.apple.com/"
    static let searchUrl = url.baseUrl + "search?term="
    static let wrapperType = "&wrappertype="
}


typealias url = URLS
