//
//  SearchFilterView.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import UIKit

class SearchFilterView: UIView, UISearchBarDelegate {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchType: UISegmentedControl!    
    weak var searchFilterDelegate : searchFilterProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        setupView()
    }
    
    func setupView() {
        self.searchBar.delegate = self
        self.searchType.replaceSegments(Statics.searchTypes)
        self.searchType.selectSegment(0)
    }
    

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isMoreThanTwo() {
            self.searchFilterDelegate?.searchText(searchText,Statics.searchTypes[self.searchType.selectedSegmentIndex])
        }
    }
    
    
    func commonInit() {
        let bundle = Bundle.init(for: SearchFilterView.self)
        guard let viewsToAdd = bundle.loadNibNamed("SearchFilterView", owner: self, options: nil), let contentView = viewsToAdd.first as? UIView  else { return }
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
}


