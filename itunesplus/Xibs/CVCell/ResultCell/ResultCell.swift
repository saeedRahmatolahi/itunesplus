//
//  ResultCell.swift
//  itunesplus
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import UIKit

class ResultCell: UICollectionViewCell {

    @IBOutlet weak var dataList: UITableView!
    var ResultData = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dataList.dataSource = self
        self.dataList.delegate = self
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.ResultData = []
        self.dataList.reloadingData()
    }

    func fetchData(_ model : Results?) {
        guard let model = model else {return}
        self.ResultData.append(model.artworkUrl100.unwrapString())
        self.ResultData.append(model.collectionPrice.unwrapDouble().localeCurrency)
        self.ResultData.append(model.collectionName.unwrapString())
        self.ResultData.append("\(model.releaseDate.unwrapString().toDate())")
        self.dataList.reloadingData()
    }
    
}


extension ResultCell : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ResultData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.setText(self.ResultData[indexPath.row])
        
        return cell
    }
    
    
}
