//
//  itunesplusTests.swift
//  itunesplusTests
//
//  Created by Saeed Rahmatolahi on 1/1/2564 BE.
//

import XCTest
@testable import itunesplus

class itunesplusTests: XCTestCase {

    func testIsMoreThan2() {
        let result = "abc".isMoreThanTwo()
        XCTAssertTrue(result)
    }
        
    func testUnwrapStr() {
        let str : String? = nil
        XCTAssertNotNil(str.unwrapString())
    }
    
    func testUnwrapDouble() {
        let str : Double? = nil
        XCTAssertNotNil(str.unwrapDouble())
    }
    
    
    func testCurrency() {
        let price : Double = 1.29
        XCTAssertEqual(price.localeCurrency, "$1.29")
    }
    
    
}
